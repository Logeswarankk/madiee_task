**Task 1**: 
  
- Linux is an open-source operating system, Linux is very  secure so many companies using linux
  1. cd - change directory command helps to change the current directory
  2. mv - move command helps to move file and we can use move command to rename the file also
  3. pwd - print working directory command displays the full pathname of the current working directory.
  4. ls - list command helps to list all files in the current directory
  5. mkdir - make directory command helps to create new folder

**Task 2** : 
- Linux kernel is the core part of the operating system. it is used to communicate between system and software

**Task 3** : 
- This type of processor tells us how much memory a processor can have access from a CPU register.
- A 32-bit system can access 4 GB of RAM and More (physical memory) but 64-bit system can access any amount of memory greater than 4 GB can be easily handled by it.

**Task 4** : 
- Git is a free, open-source distributed version control system. Git helps keep track of changes made to a code. If any  error during coding git allows us to revert back to a stable state
-  >git --version>  command is used to find the git version.

**Task 5** : 
- Gitlab account and repository created .

**Task 6** : 
- The main difference between high level language and low level language is that, Programmers can easily understand or interpret or compile the high level language in comparison of machine. On the other hand, Machine can easily understand the low level language in comparison of human beings .
- Example of high level language : C,C++, java etc
- Example of low level language : assembly and machine code

**Task 7** : 
- Objects are instances of classes created with specific data
